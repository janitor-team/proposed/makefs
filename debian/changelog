makefs (20190105-3) unstable; urgency=high

  * Team upload.
  * Require and build with recent bmake which broke its r-builddeps
    (without even so much as a notice in advance)
  * Do not pass -Wl,--as-needed (manually) any more, thanks lintian
  * Use debhelper 13
  * Update lintian overrides; add UMEGAYA metadata

 -- Thorsten Glaser <tg@mirbsd.de>  Sun, 26 Jul 2020 18:34:09 +0200

makefs (20190105-2) unstable; urgency=medium

  * Team upload.
  * Update Uploaders (name)
  * Bump Policy (no relevant changes)
  * Do not build with LTO (buggy/unreliable in GCC)
  * Slightly modernise and correct packaging
  * Define __packed, missing in glibc headers (Closes: #957516)

 -- Thorsten Glaser <tg@mirbsd.de>  Fri, 17 Apr 2020 23:45:54 +0200

makefs (20190105-1) unstable; urgency=low

  * Team upload.
  * Update to latest version from MirBSD, merges all Debian patches
    - Remove libbsd dependency, now unusable
  * Bump debhelper to 12, Policy 4.3.0 (no changes)

 -- Thorsten Glaser <tg@mirbsd.de>  Sat, 05 Jan 2019 17:53:32 +0100

makefs (20100306-7) unstable; urgency=medium

  * Convert to standard patches-unapplied format
  * Fix FTBFS with glibc 2.28 (Closes: #916004)
  * Switch from transitional pmake to bmake
  * Bump debhelper compat level to 11
  * debian/rules: Use dh sequencer and modernise
  * debian/control:
    - Update Vcs-* to point to salsa
    - Bump Standards-Version to 4.2.1; no changes needed
    - Add Rules-Requires-Root: no
    - Run wrap-and-sort -ast
    - Add myself to Uploaders

 -- James Clarke <jrtc27@debian.org>  Sun, 09 Dec 2018 21:27:46 +0000

makefs (20100306-6) unstable; urgency=medium

  * New maintainer: GNU/kFreeBSD Maintainers. (Closes: #764399)
  * Add myself to Uploaders.
  * Bump Standards-Version to 3.9.6;  no change needed.
  * Implement -T maximum-time argument, to clamp superblock and
    file timestamps to a given Epoch time, for reproducibility.

 -- Steven Chamberlain <steven@pyro.eu.org>  Wed, 11 Nov 2015 11:56:26 +0000

makefs (20100306-5) unstable; urgency=low

  * QA upload.
  * Orphan the package.

 -- Thorsten Glaser <tg@mirbsd.de>  Tue, 07 Oct 2014 18:02:14 +0000

makefs (20100306-4) unstable; urgency=low

  * Use __…__ around GCC __attribute__s
  * debian/control: Move VCS-* fields to Alioth collab-maint git
  * Remove now-useless RCS IDs
  * Bump Policy version
  * Update debian/copyright file

 -- Thorsten Glaser <tg@mirbsd.de>  Tue, 08 Jul 2014 16:25:28 +0200

makefs (20100306-3) unstable; urgency=low

  * Use debian/source/format 3.0 (quilt) with local single-debian-patch
  * Work with prospective libbsd 0.4.0 (Closes: #670183)
  * Clean up packaging; disable some gcc warnings for now

 -- Thorsten Glaser <tg@mirbsd.de>  Mon, 30 Apr 2012 16:52:02 +0000

makefs (20100306-2) unstable; urgency=low

  * makefs can do Multi-Arch: foreign as requested by vorlon
  * Add build-{arch,indep} targets (as alias to build)
  * debian/rules: cleanup (remove install/check, dh_installdirs)
  * Mention lack of Large File Support as a FIXME
  * Policy 3.9.3, no relevant changes
  * debian/rules: modernise; enable +all hardening and --as-needed
  * Use DEB_HOST_ARCH_OS to check whether we’re compiling for Hurd
  * Attempt to use Link-Time Optimisation

 -- Thorsten Glaser <tg@mirbsd.de>  Sun, 25 Mar 2012 19:43:59 +0000

makefs (20100306-1) unstable; urgency=low

  * Bump Standards-Version, no relevant change
  * debian/source/format: Enforce "1.0" manually, for now
  * debian/control: Remove DMUA, I'm a DD now
  * New upstream version, with the following change summary:
    - sync with NetBSD® upstream, gives us minor bugfixes and
      support for the Acorn Archimedes (untested)
    - support setting the {creation,modification,effective,expiry}
      date; GNU GRUB 2 uses mtime for “UUID”s…
    - add boot-info-table (J�rg compatible) support
    - get rid of MAXPATHLEN and PATH_MAX; avoid DEV_BSIZE
  * On Hurd, create an empty <sys/mount.h> (Closes: #562297)

 -- Thorsten Glaser <tg@mirbsd.de>  Sun, 07 Mar 2010 00:42:21 +0000

makefs (20091122-1) unstable; urgency=low

  * New upstream release, summary of changes:
    - Fixes to the bundled manual pages (e.g. - → \-)
  * Add README.Debian containing some notes on this programme

 -- Thorsten Glaser <tg@mirbsd.de>  Tue, 15 Dec 2009 11:15:31 +0000

makefs (20090808-2) unstable; urgency=low

  * debian/rules: provide __RCSID if libbsd (< 0.1.3) doesn’t, helps
    backports and avr32; Closes: #547399
  * remove lintian overrides no longer necessary
  * bump Standards-Version 3.8.2 -> 3.8.3, no relevant changes

 -- Thorsten Glaser <tg@mirbsd.de>  Sat, 26 Sep 2009 16:57:42 +0000

makefs (20090808-1) unstable; urgency=low

  * Initial release (Closes: #538171)

 -- Thorsten Glaser <tg@mirbsd.de>  Sat, 08 Aug 2009 13:46:39 +0000
