# $MirOS: src/usr.sbin/Makefile.inc,v 1.5 2019/01/05 16:02:26 tg Exp $
# $OpenBSD: Makefile.inc,v 1.2 1997/09/21 11:43:04 deraadt Exp $

.ifndef _MODSRC_USR_SBIN_MAKEFILE_INC
_MODSRC_USR_SBIN_MAKEFILE_INC=1

BINDIR?=	/usr/sbin
.ifndef GNUPORT
LDFLAGS+=	-Wl,-rpath -Wl,/usr/lib
.endif

.endif
